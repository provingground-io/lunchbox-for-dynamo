﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Xml;

using Autodesk.DesignScript.Runtime;
using Autodesk.DesignScript.Geometry;

using Newtonsoft.Json;

namespace Lists
{
    /// <summary>
    /// Manage Lists
    /// </summary>
    public static class Manage
    {
        /// <summary>
        /// Removes null items from a list
        /// </summary>
        /// <param name="Data">Data with nulls</param>
        /// <returns path="Cleaned">Cleaned data.</returns>
        /// <returns path="Indices">Index map of cleaned data.</returns>
        /// <search>lunchbox,lists,duplicate,clean,null,remove</search>
        [MultiReturn(new[] { "Cleaned", "Indices" })]
        public static Dictionary<string, object> RemoveNulls(List<object> Data)
        {
            List<object> m_data = Data;
            List<int> m_indices = new List<int>();
            for (int i = 0; i < m_data.Count; i++)
            {
                if (m_data[i] == null)
                {
                    m_indices.Add(i);
                }
            }

            m_data.RemoveAll(item => item == null);

            return new Dictionary<string, object>
      {
        {"Cleaned", m_data},
        {"Indices", m_indices}
      };
        }

        /// <summary>
        /// Replace Null values with something else
        /// </summary>
        /// <param name="Data">Data to check</param>
        /// <param name="ReplaceWith">Replace nulls with this value</param>
        /// <returns name="Replaced">New items with replaced nulls</returns>
        /// <search>lunchbox,lists,null,replace</search>
        public static object ReplaceNulls(object Data, object ReplaceWith)
        {

            object m_newdata;
            if (Data == null)
            {
                m_newdata = ReplaceWith;
            }
            else
            {
                m_newdata = Data;
            }

            return m_newdata;
        }

        /// <summary>
        /// Scramble the order of a list of data
        /// </summary>
        /// <param name="Data">Data</param>
        /// <param name="Seed">Random seed</param>
        /// <returns name="Randomized">Scrambled order.</returns>
        /// <returns name="Indices">Index map of scrambled data.</returns>
        /// <search>lunchbox,lists,random,order</search>
        [MultiReturn(new[] { "Randomized", "Indices" })]
        public static Dictionary<string, object> RandomizeOrder(List<object> Data, int Seed)
        {
            Random m_random = new Random(Seed);
            List<double> m_vals = new List<double>();

            List<int> m_indices = new List<int>();

            int i = 0;
            foreach (object d in Data)
            {
                double val = m_random.NextDouble();
                m_vals.Add(val);

                m_indices.Add(i);

                i++;
            }

            int[] ind = m_indices.ToArray();
            object[] objarr = Data.ToArray();

            Array.Sort(m_vals.ToArray(), objarr);
            Array.Sort(m_vals.ToArray(), ind);

            return new Dictionary<string, object>
      {
        {"Randomized", objarr},
        {"Indices", ind}
      };
        }
    }

    /// <summary>
    /// Create and manage dictionary objects
    /// </summary>
    public static class Dictionary
    {
        /// <summary>
        /// Create a new dictionary using key value pairs
        /// </summary>
        /// <param name="Keys">Unique keys for the dictionary</param>
        /// <param name="Values">Values (objects)</param>
        /// <returns name="Dictionary">Dictionary</returns>
        /// <serach>lunchbox,dictionary,search</serach>
        [MultiReturn(new[] { "Dictionary" })]
        public static Dictionary<string, object> CreateDictionary(List<string> Keys, List<object> Values)
        {
            Dictionary<string, object> m_dictionary = new Dictionary<string, object>();

            for (int i = 0; i < Keys.Count; i++)
            {
                m_dictionary.Add(Keys[i], Values[i]);
            }

            LunchBoxClasses.Dictionary m_custdict = new LunchBoxClasses.Dictionary(m_dictionary);

            return new Dictionary<string, object>
      {
        {"Dictionary", m_custdict}
      };
        }

        /// <summary>
        /// Gets the Keys and Values in a Dictionary
        /// </summary>
        /// <param name="dictionary">Dictionary object</param>
        /// <returns name="Keys">Dictionary keys.</returns>
        /// <returns name="Values">Dictionary values.</returns>
        /// <search>lunchbox,dictionary,search</search>
        [MultiReturn(new[] { "Keys", "Values" })]
        public static Dictionary<string, object> GetDictionaryKeysValues(object dictionary)
        {
            List<string> m_keys = null;
            List<object> m_vals = null;

            if (dictionary is LunchBoxClasses.Dictionary)
            {
                LunchBoxClasses.Dictionary m_dict = (LunchBoxClasses.Dictionary)dictionary;
                m_keys = m_dict.diction.Keys.ToList();
                m_vals = m_dict.diction.Values.ToList();
            }


            return new Dictionary<string, object>
      {
        {"Keys", m_keys},
        {"Values", m_vals}
      };
        }

        /// <summary>
        /// Gets a value from a dictionary
        /// </summary>
        /// <param name="dictionary">Dictionary to search</param>
        /// <param name="Key">Key</param>
        /// <returns name="Values">Value from dictionary.</returns>
        /// <search>lunchbox,dictionary,search</search>
        [MultiReturn(new[] { "Values" })]
        public static Dictionary<string, object> GetValueFromDictionary(object dictionary, String Key)
        {
            object obj = null;
            if (dictionary is LunchBoxClasses.Dictionary)
            {
                LunchBoxClasses.Dictionary m_dict = (LunchBoxClasses.Dictionary)dictionary;
                m_dict.diction.TryGetValue(Key, out obj);
            }

            return new Dictionary<string, object>
      {
        {"Values", obj}
      };
        }
    }

    /// <summary>
    /// Create and Manage Sets of Data
    /// </summary>
    public static class DataSets
    {
        /// <summary>
        /// Creates a new DataTable
        /// </summary>
        /// <param name="Name">Name of the DataTable</param>
        /// <param name="Headers">List of column headers</param>
        /// <param name="Data">Data organized by rows</param>
        /// <returns name="DataTable">DataTable</returns>
        [MultiReturn(new[] { "DataTable" })]
        public static Dictionary<string, object> CreateDataTable(string Name, List<string> Headers, List<List<object>> Data)
        {
            DataTable m_dt = new DataTable(Name);
            foreach (string h in Headers)
            {
                m_dt.Columns.Add(h);
            }

            foreach (List<object> d in Data)
            {
                DataRow dr = m_dt.NewRow();
                for (int i = 0; i < d.Count; i++)
                {
                    dr[i] = d[i];
                }
                m_dt.Rows.Add(dr);
            }

            return new Dictionary<string, object>
      {
        {"DataTable", m_dt}
      };
        }

        /// <summary>
        /// Create a new DataSet
        /// </summary>
        /// <param name="Name">Name of the DataSet</param>
        /// <param name="Tables">DataTables in the DataSet</param>
        /// <returns name="DataSet">DataSet</returns>
        /// <search>lunchbox,lists,dataset</search>
        [MultiReturn(new[] { "DataSet" })]
        public static Dictionary<string, object> CreateDataSet(string Name, List<DataTable> Tables)
        {
            DataSet m_ds = new DataSet(Name);
            foreach (DataTable t in Tables)
            {
                DataTable dtcopy = t.Copy();

                m_ds.Tables.Add(dtcopy);
            }

            return new Dictionary<string, object>
      {
        {"DataSet", m_ds}
      };
        }

        /// <summary>
        /// Get a list of tables from a DataSet
        /// </summary>
        /// <param name="Data">DataSet</param>
        /// <returns name="DataTables">List of DataTables Objects.</returns>
        /// <returns name="Names">List of DataTable names.</returns>
        /// <search>lunchbox,lists,datatable,dataset</search>
        [MultiReturn(new[] { "DataTables", "Names" })]
        public static Dictionary<string, object> GetTablesfromDataSet(DataSet Data)
        {
            List<DataTable> m_dts = null;
            List<string> m_names = null;
            foreach (DataTable dt in Data.Tables)
            {
                m_dts.Add(dt);
                m_names.Add(dt.TableName);
            }

            return new Dictionary<string, object>
      {
        {"DataTables", m_dts},
        {"Names", m_names}
      };
        }

        /// <summary>
        /// Get all rows from a DataTable
        /// </summary>
        /// <param name="Data">DataTable</param>
        /// <returns name="Rows">Data organized by Row</returns>
        /// <returns name="Headers">Table headers.</returns>
        /// <search>lunchbox,lists,rows,dataset,datatable</search>
        [MultiReturn(new[] { "Rows", "Headers" })]
        public static Dictionary<string, object> GetDataFromTable(DataTable Data)
        {
            int m_rowscount = Data.Rows.Count;
            int m_colscount = Data.Columns.Count;

            // Parse DataSet
            object[][] m_outarr = new object[m_rowscount][];
            List<string> m_headers = new List<string>();

            for (int i = 0; i <= m_rowscount - 1; i++)
            {
                m_outarr[i] = new object[m_colscount];
                for (int j = 0; j <= m_colscount - 1; j++)
                {
                    m_outarr[i][j] = Data.Rows[i][j];
                }
            }

            foreach (DataColumn c in Data.Columns)
            {
                m_headers.Add(c.ColumnName);
            }

            return new Dictionary<string, object>
      {
        {"Rows", m_outarr},
        {"Headers", m_headers}
      };
        }

        /// <summary>
        /// Search a DataTable using DataSet 'Select' Syntax.
        /// </summary>
        /// <param name="Data"></param>
        /// <param name="SelectString">Criteria to search for.</param>
        /// <returns name="DataTable">DataTable with the rows that meet the criteria</returns>
        [MultiReturn(new[] { "DataTable" })]
        public static Dictionary<string, object> SelectDataFromTable(DataTable Data, string SelectString)
        {
            DataTable m_newtable = Data.Clone();
            DataRow[] m_rows = Data.Select(SelectString);

            foreach (DataRow r in m_rows)
            {
                m_newtable.ImportRow(r);
            }

            return new Dictionary<string, object>
            {
                {"DataTable", m_newtable}
            };
        }

        /// <summary>
        /// Convert a DataSet to XML
        /// </summary>
        /// <param name="Data">DataSet object</param>
        /// <param name="SaveBool">Set to 'true' to save the file.</param>
        /// <param name="FilePath">File path of the saved file.</param>
        /// <returns name="XML">XML</returns>
        /// <search>lunchbox,lists,rows,dataset,datatable, XML</search>
        [MultiReturn(new[] { "XML" })]
        public static Dictionary<string, object> SerializeXML(DataSet Data, bool SaveBool, string FilePath)
        {
            XmlDocument m_XmlDoc = new XmlDocument();
            m_XmlDoc.LoadXml(Data.GetXml());

            string m_XmlString = Data.GetXml();
            if (SaveBool == true)
            {
                m_XmlDoc.Save(FilePath);
            }

            return new Dictionary<string, object>
      {
        {"XML", m_XmlString}
      };
        }

        /// <summary>
        /// Convert a DataSet to JSON
        /// </summary>
        /// <param name="Data">DataSet object</param>
        /// <param name="SaveBool">Set to 'true' to save the file.</param>
        /// <param name="FilePath">File path of the saved file.</param>
        /// <returns name="JSON">JSON</returns>
        /// <search>lunchbox,lists,rows,dataset,datatable,JSON,XML</search>
        [MultiReturn(new[] { "JSON"})]
        public static Dictionary<string, object> SerializeJSON(DataSet Data, bool SaveBool, string FilePath)
        {

            string m_json = Newtonsoft.Json.JsonConvert.SerializeObject(Data, Newtonsoft.Json.Formatting.Indented, new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });

            if (SaveBool == true)
            {
                StreamWriter writer = new System.IO.StreamWriter(FilePath, false);
                writer.Write(m_json);
            }

            return new Dictionary<string, object>
      {
        {"XML", m_json}
      };
        }

        /// <summary>
        /// Convert XML to Dynamo
        /// </summary>
        /// <param name="XML">XML string</param>
        /// <returns name="Values">Node values.</returns>
        /// <returns name="Nodes">Node names.</returns>
        /// <search>lunchbox,lists,rows,dataset,datatable, XML</search>
        [MultiReturn(new[] { "Values", "Nodes" })]
        public static Dictionary<string, object> DeserializeXML(string XML)
        {
            XmlDocument m_xmldoc = new XmlDocument();
            m_xmldoc.LoadXml(XML);

            LunchBoxClasses.clsXMLHelper m_xmlhelper = new LunchBoxClasses.clsXMLHelper(m_xmldoc);

            return new Dictionary<string, object>
      {
        {"Values", m_xmlhelper.Values},
        {"Nodes", m_xmlhelper.Nodes}
      };
        }

        /// <summary>
        /// Get XML node properties
        /// </summary>
        /// <param name="XML"></param>
        /// <param name="TagName"></param>
        /// <returns name="InnerXML">XML contained within the tag</returns>
        [MultiReturn(new[] { "InnerXML" })]
        public static Dictionary<string, object> GetXMLNode(string XML, string TagName)
        {
            XmlDocument m_xmldoc = new XmlDocument();
            m_xmldoc.LoadXml(XML);

            XmlNodeList m_nodelist = m_xmldoc.GetElementsByTagName(TagName);
            List<string> m_innertext = new List<string>();
            List<string> m_innerxml = new List<string>();

            foreach (XmlNode x in m_nodelist)
            {
                m_innertext.Add(x.InnerText);
                m_innerxml.Add(x.InnerXml);
            }

            return new Dictionary<string, object>
      {
      {"InnerXML", m_innerxml}
      };
        }

        /// <summary>
        /// Convert XML to JSON
        /// </summary>
        /// <param name="XML">XML String</param>
        /// <returns name="JSON">JSON String</returns>
        /// <search>lunchbox,lists,rows,dataset,datatable,JSON,XML</search>
        [MultiReturn(new[] { "JSON" })]
        public static Dictionary<string, object> XMLtoJSON(string XML)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(XML);

            string m_json = Newtonsoft.Json.JsonConvert.SerializeXmlNode(xmlDoc);

            return new Dictionary<string, object>
            {
                {"JSON", m_json}
            };
        }

        /// <summary>
        /// Convert JSON to XML
        /// </summary>
        /// <param name="JSON">JSON String</param>
        /// <returns name="XML">XML String</returns>
        /// <search>lunchbox,lists,rows,dataset,datatable,JSON,XML</search>
        [MultiReturn(new[] { "XML" })]
        public static Dictionary<string, object> JSONtoXML(string JSON)
        {
            XmlDocument m_xmlDoc = Newtonsoft.Json.JsonConvert.DeserializeXmlNode(JSON);
            StringWriter m_sw = new StringWriter();
            XmlTextWriter m_xw = new XmlTextWriter(m_sw);

            m_xmlDoc.WriteTo(m_xw);

            return new Dictionary<string, object>
            {
                {"JSON", m_sw.ToString()}
            };
        }
    }

    /// <summary>
    /// Create sequences
    /// </summary>
    public static class Sequence
    {

        /// <summary>
        /// Random list of numbers using Min/Max range, number of items, and seed value
        /// </summary>
        /// <param name="Min">Minimum Value in Range of Random Numbers</param>
        /// <param name="Max">Maximum Value in Range of Random Numbers</param>
        /// <param name="Number">Number of Random Numbers</param>
        /// <param name="Seed">Random Seed Value</param>
        /// <returns name="Numbers">List of numbers</returns>
        /// <search>lunchbox,lists,random,numbers,sequence,division</search>
        public static List<double> RandomNumbers(double Min, double Max, double Number, int Seed)
        {
            double m_range = Max - Min;
            Random m_random = new Random(Seed);

            List<double> m_randlist = new List<double>();
            for (int i = 0; i < Number; i++)
            {
                double m_num = m_random.NextDouble();
                double m_scaled = Min + (m_num * m_range);
                m_randlist.Add(m_scaled);
            }

            return m_randlist;
        }

        /// <summary>
        /// A range of numbers based on divisions
        /// </summary>
        /// <param name="Start">The starting value of the range</param>
        /// <param name="End">The ending value of the range</param>
        /// <param name="Division">Number of divisions for the range</param>
        /// <returns name="Numbers">List of numbers</returns>
        /// <search>lunchbox,lists,range,numbers,sequence,division</search>
        public static List<double> RangeDivision(double Start, double End, int Division)
        {
            try
            {
                double m_step = Math.Abs(Start - End) / Division;

                List<double> m_numbers = new List<double>();

                double inc = Start;
                while (inc <= End)
                {
                    m_numbers.Add(inc);
                    inc = inc + m_step;
                }

                return m_numbers;
            }
            catch { return null; }
        }
    }

    /// <summary>
    /// Manipulate Strings
    /// </summary>
    public static class Strings
    {
        /// <summary>
        /// Sorts a list of strings based on duplicate values
        /// </summary>
        /// <param name="Strings">List of strings to sort through</param>
        /// <returns name="UniqueStrings">Sorted duplicates</returns>
        /// <returns name="Indices">Index map of the unique strings.</returns>
        /// <search>lunchbox,lists,search,duplicate,sort,string</search>
        [MultiReturn(new[] { "UniqueStrings", "Indices" })]
        public static Dictionary<string, object> SortDuplicateStrings(List<string> Strings)
        {
            Dictionary<string, string> m_diction = new Dictionary<string, string>();
            for (int i = 0; i < Strings.Count; i++)
            {
                if (m_diction.ContainsKey(Strings[i]))
                {
                    m_diction[Strings[i]] = m_diction[Strings[i]] + "," + i.ToString();
                }
                else
                {
                    m_diction.Add(Strings[i], i.ToString());
                }
            }

            List<List<int>> m_indices = new List<List<int>>();
            for (int i = 0; i < m_diction.Values.Count; i++)
            {
                List<int> m_indexlist = new List<int>();
                List<string> m_valstrings = m_diction.Values.ToList();
                string[] m_strarr = m_valstrings[i].Split(',');

                foreach (string s in m_strarr)
                {
                    int val = Convert.ToInt16(s);
                    m_indexlist.Add(val);
                }

                m_indices.Add(m_indexlist);
            }

            return new Dictionary<string, object>
      {
        {"UniqueStrings", m_diction.Keys},
        {"Indices", m_indices}
      };
        }

        /// <summary>
        /// Search through a list of strings
        /// </summary>
        /// <param name="Strings">List of strings to search through</param>
        /// <param name="SearchFor">Item to search for</param>
        /// <returns name="Strings">List of found items</returns>
        /// <returns name="Indices">Index map of the found strings.</returns>
        /// <search>lunchbox,lists,search,string</search>
        [MultiReturn(new[] { "Strings", "Indices" })]
        public static Dictionary<string, object> SearchListForString(List<string> Strings, string SearchFor)
        {
            List<string> m_founditems = new List<string>();
            List<int> m_foundindices = new List<int>();

            int i = 0;
            foreach (string str in Strings)
            {
                if (str == SearchFor)
                {
                    m_founditems.Add(str);
                    m_foundindices.Add(i);
                }
                i++;
            }

            return new Dictionary<string, object>
      {
        {"Strings", m_founditems},
        {"Indices", m_foundindices}
      };
        }
    }
}
