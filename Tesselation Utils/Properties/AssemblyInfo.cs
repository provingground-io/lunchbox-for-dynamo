﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ProvingGround.Tesselation")]
[assembly: AssemblyDescription("Tesselation algorithms")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Proving Ground")]
[assembly: AssemblyProduct("ProvingGround.Tesselation")]
[assembly: AssemblyCopyright("Copyright ©  2020 PROVING GROUND LLC")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("6c77d1dc-19dd-4441-91c0-faf0e94ecca3")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2016.4.30.0")]
[assembly: AssemblyFileVersion("2016.4.30.0")]
